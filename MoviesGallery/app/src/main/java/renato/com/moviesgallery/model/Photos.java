package renato.com.moviesgallery.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Renato on 25/06/2015.
 */
public class Photos {
    @Expose
    private int page;
    @Expose
    private int pages;
    @Expose
    private int perpage;
    @Expose
    private int total;
    @Expose
    @SerializedName("photo")
    private List<Photo> listaPhotos;



    public List<Photo> getPhotos() {
        return listaPhotos;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getPerpage() {
        return perpage;
    }

    public void setPerpage(int perpage) {
        this.perpage = perpage;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Photo> getListaPhotos() {
        return listaPhotos;
    }

    public void setListaPhotos(ArrayList<Photo> listaPhotos) {
        this.listaPhotos = listaPhotos;
    }
}

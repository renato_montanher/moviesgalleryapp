package renato.com.moviesgallery;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;

import java.util.ArrayList;
import java.util.List;

import renato.com.moviesgallery.controller.PhotoController;
import renato.com.moviesgallery.dialog.InfoDialog_;
import renato.com.moviesgallery.listener.EndlessScrollListener;
import renato.com.moviesgallery.adapter.ListImageMovieAdapter;
import renato.com.moviesgallery.model.Photo;


@EActivity(R.layout.activity_inicio)
public class Inicio extends FragmentActivity {

    @ViewById
    ListView listviewPhotos;

    @Bean
    PhotoController controller;

    @StringRes
    String problemasNoRequest,erroNoServidor;


    @Bean
    ListImageMovieAdapter adapter;

    int pagina = 0;

    private List<Photo> listaPhotos = new ArrayList<>();

    @AfterViews
    void afterView() {
        listviewPhotos.setAdapter(adapter);
        listviewPhotos.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                pagina += 1;
                Toast.makeText(getApplicationContext(), "carregando..",Toast.LENGTH_SHORT).show();
                controller.buscaPhotos(pagina);
            }
        });
        listviewPhotos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {


                Intent it = new Intent(getApplicationContext(), Detalhes_.class);
                it.putExtra("id_photo", getListaPhotos().get(position).getId());
                it.putExtra("url_photo", getListaPhotos().get(position).getUrlPhoto());
                it.putExtra("titulo", getListaPhotos().get(position).getTitle());
                startActivity(it);

            }
        });
    }

    @AfterInject
    void afterInject() {

    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
    }

    public void atualizaTelaComResultado(List<Photo> photos) {
        listaPhotos.addAll(photos);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.inicio, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void trataErrosDeConexao() {

    }

    public void trataErrosDeHTTP(int status) {
        String mensagemId;

        if (status == 400)
            mensagemId = problemasNoRequest;
        else
            mensagemId = erroNoServidor;

        InfoDialog_.builder().infoMessage(mensagemId).build()
                .show(getFragmentManager(), "info_dialog");

    }


    public List<Photo> getListaPhotos() {
        return listaPhotos;
    }
}

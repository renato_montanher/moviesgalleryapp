package renato.com.moviesgallery;

import android.app.Activity;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;


import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import renato.com.moviesgallery.adapter.ListImageMovieAdapter;
import renato.com.moviesgallery.model.Photo;


@EFragment(R.layout.fragment_imagemovies_list)
public class ImageMoviesFragment extends Fragment implements AdapterView.OnItemClickListener{

    Inicio activity;

    ListView listviewPhotos;

    ListImageMovieAdapter mAdapter;


    public List<Photo> listaPhotos = new ArrayList<Photo>();
    public List<Photo> getListaDePhotos() {
        return listaPhotos;
    }

    public ImageMoviesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    //metodo deve ser chamado depois da injecao de views
    @AfterViews
    void preencherViewList(){
       /*
        listaPhotos.add(new Photo(1, "dono 1", "titulo1"));
        listaPhotos.add(new Photo(2, "dono 2", "titulo2"));
        */
        Toast.makeText(this.getActivity(),"carregado", Toast.LENGTH_SHORT).show();

        // listviewPhotos.setOnItemClickListener(this);
        mAdapter = new ListImageMovieAdapter();
        listviewPhotos.setAdapter(mAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return null;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
           // mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }


    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
      //  if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
           // mListener.onFragmentInteraction(DummyContent.ITEMS.get(position).id);
       // }
    }






}

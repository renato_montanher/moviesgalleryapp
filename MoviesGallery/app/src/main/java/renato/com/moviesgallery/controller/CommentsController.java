package renato.com.moviesgallery.controller;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.UiThread;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import renato.com.moviesgallery.Detalhes;
import renato.com.moviesgallery.Inicio;
import renato.com.moviesgallery.R;
import renato.com.moviesgallery.model.Comment;
import renato.com.moviesgallery.model.Photo;
import renato.com.moviesgallery.model.ResponsePhotos;
import renato.com.moviesgallery.model.ResponsePhotosComments;
import renato.com.moviesgallery.model.ResponsePhotosInfo;
import retrofit.ErrorHandler;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;
import retrofit.http.GET;
import retrofit.http.QueryMap;

/**
 * Created by Renato on 29/06/2015.
 */
@EBean
public class CommentsController implements ErrorHandler {
    private static final String TAG = PhotoController.class.getSimpleName();

    @RootContext
    Detalhes activity;

    private ConsultaCommentssApi api;

    @AfterInject
    void afterInject() {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
        api = new RestAdapter.Builder()
                .setConverter(new GsonConverter(gson))
                .setClient(new OkClient())
                .setEndpoint("https://api.flickr.com/services/rest/")
                .setErrorHandler(this)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build().create(ConsultaCommentssApi.class);

    }

    @Override
    public Throwable handleError(RetrofitError cause) {

        if (cause.getKind() == RetrofitError.Kind.NETWORK)
            activity.trataErrosDeConexao();

        else if (cause.getKind() == RetrofitError.Kind.HTTP)
            activity.trataErrosDeHTTP(cause.getResponse().getStatus());

        return cause;
    }

    @UiThread
    void atualiza(List<Comment> listComment) {
        activity.atualizaTelaComResultado(listComment);
    }

    @Background
    public void buscaPhotoInfo(String id) {

        try {
            Map<String, String> map = new HashMap<>();
            map.put("method", "flickr.photos.getInfo");
            map.put("api_key", activity.getResources().getString(R.string.flickr_app_id));
            map.put("photo_id", id);
            map.put("format", "json");
            map.put("nojsoncallback", "1");


           // final ResponsePhotos resposta = api.buscaPhotoComments(map);

           // atualiza(resposta.getPhotos().getListaPhotos());
        } catch (Exception e) {
            Log.e(TAG, "Erro ao procurar Shot", e);
        }
    }


    @Background
    public void buscaPhotoComments(String id) {

        try {
            Map<String, String> map = new HashMap<>();
            map.put("method", "flickr.photos.comments.getList");
            map.put("api_key", activity.getResources().getString(R.string.flickr_app_id));
            map.put("photo_id", id);
            map.put("format", "json");
            map.put("nojsoncallback", "1");


            final ResponsePhotosComments resposta = api.buscaPhotoComments(map);

            atualiza(resposta.getComments().getComments());
        } catch (Exception e) {
            Log.e(TAG, "Erro ao procurar Shot", e);
        }
    }

    public static interface ConsultaCommentssApi {

        @GET("/")
        ResponsePhotosInfo buscaPhotoInfo(@QueryMap Map<String, String> options);

        @GET("/")
        ResponsePhotosComments buscaPhotoComments(@QueryMap Map<String, String> options);
    }
}
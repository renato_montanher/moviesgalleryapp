package renato.com.moviesgallery.model;

import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by Renato on 25/06/2015.
 */
public class Comments {
    @Expose
    long photo_id;
    @Expose
    List<Comment> comment ;
    @Expose
    String stat;

    public long getPhoto_id() {
        return photo_id;
    }

    public void setPhoto_id(long photo_id) {
        this.photo_id = photo_id;
    }

    public List<Comment> getComments() {
        return comment;
    }

    public void setComments(List<Comment> comments) {
        this.comment = comments;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }
}

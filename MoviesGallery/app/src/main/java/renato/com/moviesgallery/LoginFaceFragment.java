package renato.com.moviesgallery;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.Arrays;

import de.greenrobot.event.EventBus;

@EFragment(R.layout.fragment_login_face)
public class LoginFaceFragment extends Fragment {
    @ViewById
    LoginButton loginButton;

    CallbackManager callbackManager;

    ProfileTracker profileTracker;


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    public void onDestroy() {
        super.onDestroy();
        profileTracker.stopTracking();
    }


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getActivity());


        callbackManager = CallbackManager.Factory.create();
        /*
        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(
                    Profile oldProfile,
                    Profile currentProfile) {
                    String s = currentProfile.getFirstName();
                    String x = currentProfile.getMiddleName();
            }
        };
        */

    }
    @AfterViews
    void calledAfterViewInjection() {
        loginButton.setReadPermissions(Arrays.asList("user_friends", "email", "public_profile"));

        // If using in a fragment
        loginButton.setFragment(this);
        // Other app specific specialization
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        EventBus.getDefault().post(loginResult);
                    }

                    @Override
                    public void onCancel() {
                        Log.i("face", "desconectado!!");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.i("face", "erro!!");
                    }
                });

    }

    public View onCreateView(
        LayoutInflater inflater,
        ViewGroup container,
        Bundle savedInstanceState) {
    callbackManager = CallbackManager.Factory.create();

    // Callback registration
    return null;
}
}

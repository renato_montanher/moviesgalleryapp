package renato.com.moviesgallery.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Renato on 28/06/2015.
 */
public class PhotoInfo {
    @Expose
    private long  id;
    @Expose
    private Owner owner;
    @Expose
    private String secret;
    @Expose
    private  String server;
    @Expose
    private String farm;
    @Expose
    private String title;
    @Expose
    int ispublic;
    @Expose
    int isfriend;
    @Expose
    int isfamily;
}

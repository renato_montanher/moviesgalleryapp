package renato.com.moviesgallery.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Renato on 25/06/2015.
 */
public class Photo {
    @Expose
    private long  id;
    @Expose
    private String owner;
    @Expose
    private String secret;
    @Expose
    private  String server;
    @Expose
    private String farm;
    @Expose
    private String title;
    @Expose
    int ispublic;
    @Expose
    int isfriend;
    @Expose
    int isfamily;

/*
    public Photo(long id, String owner, String title) {
        this.id = id;
        this.owner = owner;
        this.title = title;
    }
*/
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getFarm() {
        return farm;
    }

    public void setFarm(String farm) {
        this.farm = farm;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIspublic() {
        return ispublic;
    }

    public void setIspublic(int ispublic) {
        this.ispublic = ispublic;
    }

    public int getIsfriend() {
        return isfriend;
    }

    public void setIsfriend(int isfriend) {
        this.isfriend = isfriend;
    }

    public int getIsfamily() {
        return isfamily;
    }

    public void setIsfamily(int isfamily) {
        this.isfamily = isfamily;
    }

    public String getUrlPhoto (){
        return "https://farm"+getFarm()+".staticflickr.com/"+getServer()+"/"+getId()+"_"+getSecret()+"_m.jpg";
    }
}

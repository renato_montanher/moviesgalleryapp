package renato.com.moviesgallery.api;

import java.util.HashMap;
import java.util.Map;

import retrofit.RestAdapter;

/**
 * Created by Renato on 26/06/2015.
 */
public class ApiRequester {
    RestAdapter restAdapter;

    public ApiRequester(){
        restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://api.flickr.com/services/rest")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
    }
    //funcionando!
    public void requestImagemMovies(){
        FlickrApi api = restAdapter.create(FlickrApi.class);
        //pesuisa com tag moviesm
        Map<String, String> map = new HashMap<>();
        map.put("method","flickr.photos.search");
        map.put("api_key","4ceebcb1f06336291251120558283a64");
        map.put("tags","movies");
        map.put("format","json");
        map.put("nojsoncallback","1");

        api.buscarImagemTagMovies(map, new RequestListenerListaPhotos());
    }
}

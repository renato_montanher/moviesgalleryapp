package renato.com.moviesgallery.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import renato.com.moviesgallery.ImageMoviesFragment;
import renato.com.moviesgallery.Inicio;
import renato.com.moviesgallery.R;
import renato.com.moviesgallery.model.Photo;

/**
 * Created by Renato on 22/10/2014.
 */
@EBean
public class ListImageMovieAdapter extends BaseAdapter {
    @RootContext
    Inicio activity;

    protected int serverListSize = -1;
    public static final int VIEW_TYPE_LOADING = 0;
    public static final int VIEW_TYPE_ACTIVITY = 1;

    public void setServerListSize(int serverListSize){
        this.serverListSize = serverListSize;
    }

    public boolean isEnabled(int position) {

        return getItemViewType(position) == VIEW_TYPE_ACTIVITY;
    }

    public int getViewTypeCount() {
        return 2;
    }

    public int getCount() {
        return activity.getListaPhotos().size() + 1;
    }
    public int getItemViewType(int position) {
        // TODO Auto-generated method stub
        return (position >= activity.getListaPhotos().size()) ? VIEW_TYPE_LOADING
                : VIEW_TYPE_ACTIVITY;
    }

    public Photo getItem(int position) {
        // TODO Auto-generated method stub
        return (getItemViewType(position) == VIEW_TYPE_ACTIVITY) ? activity.getListaPhotos()
                .get(position) : null;
    }


    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return (getItemViewType(position) == VIEW_TYPE_ACTIVITY) ? position
                : -1;
    }

    public  View getView(int position, View convertView, ViewGroup parent){
        if (getItemViewType(position) == VIEW_TYPE_LOADING) {
            // display the last row
            return getFooterView(position, convertView, parent);
        }
        View dataRow = convertView;
        dataRow = getDataRow(position, convertView, parent);

        return dataRow;
    };




    public View getDataRow(int posicao, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);

        View item = inflater.inflate(R.layout.item_photo_movies, null);
        Photo photo = activity.getListaPhotos().get(posicao);

        TextView autor = (TextView) item.findViewById(R.id.autor);
        TextView tituloFoto = (TextView) item.findViewById(R.id.photo_title);
        Uri uri = Uri.parse(photo.getUrlPhoto());
        SimpleDraweeView draweeView = (SimpleDraweeView) item.findViewById(R.id.foto_movies);
        draweeView.setImageURI(uri);

        tituloFoto.setText(photo.getTitle());
        autor.setText(photo.getOwner());
        return item;
    }

    static class ViewHolderLinha{
        SimpleDraweeView draweeView;
        TextView tituloFoto;
        TextView autor;
    }


    public View getFooterView(int position, View convertView,
                              ViewGroup parent) {
        if (position >= serverListSize && serverListSize > 0) {
            // the ListView has reached the last row
            TextView tvLastRow = new TextView(activity);
            tvLastRow.setHint("Reached the last row.");
            tvLastRow.setGravity(Gravity.CENTER);
            return tvLastRow;
        }

        View row = convertView;
        if (row == null) {
            row = activity.getLayoutInflater().inflate(
                    R.layout.progress, parent, false);
        }

        return row;
    }

}

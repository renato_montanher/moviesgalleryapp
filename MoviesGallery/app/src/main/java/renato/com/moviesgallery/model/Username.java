package renato.com.moviesgallery.model;

/**
 * Created by Renato on 25/06/2015.
 */
public class Username {
    String _content;

    public String get_content() {
        return _content;
    }

    public void set_content(String _content) {
        this._content = _content;
    }
}

package renato.com.moviesgallery.api;

import java.util.List;

import de.greenrobot.event.EventBus;
import renato.com.moviesgallery.model.Photo;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Renato on 26/06/2015.
 */
public class RequestListenerListaPhotos implements Callback<List<Photo>> {

     public void success(List<Photo> Photos, Response response) {
        EventBus.getDefault().post(Photos);
    }

     public void failure(RetrofitError error) {
        EventBus.getDefault().post(error);
    }
}

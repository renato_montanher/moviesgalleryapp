package renato.com.moviesgallery.adapter;

import android.net.Uri;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import renato.com.moviesgallery.Detalhes;
import renato.com.moviesgallery.R;
import renato.com.moviesgallery.model.Comment;
import renato.com.moviesgallery.model.Photo;

/**
 * Created by Renato on 28/06/2015.
 */
@EBean
public class ListComentariosAdapter extends BaseAdapter {
    @RootContext
    Detalhes activity;

    protected int serverListSize = -1;
    public static final int VIEW_TYPE_LOADING = 0;
    public static final int VIEW_TYPE_ACTIVITY = 1;

    public void setServerListSize(int serverListSize){
        this.serverListSize = serverListSize;
    }

    public boolean isEnabled(int position) {

        return getItemViewType(position) == VIEW_TYPE_ACTIVITY;
    }

    public int getViewTypeCount() {
        return 2;
    }

    public int getCount() {
        return activity.getListaCommentarios().size() + 1;
    }
    public int getItemViewType(int position) {
        // TODO Auto-generated method stub
        return (position >= activity.getListaCommentarios().size()) ? VIEW_TYPE_LOADING
                : VIEW_TYPE_ACTIVITY;
    }

    public Comment getItem(int position) {
        // TODO Auto-generated method stub
        return (getItemViewType(position) == VIEW_TYPE_ACTIVITY) ? activity.getListaCommentarios()
                .get(position) : null;
    }


    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return (getItemViewType(position) == VIEW_TYPE_ACTIVITY) ? position
                : -1;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        if (getItemViewType(position) == VIEW_TYPE_LOADING) {
            // display the last row
            return getFooterView(position, convertView, parent);
        }
        View dataRow = convertView;
        dataRow = getDataRow(position, convertView, parent);

        return dataRow;
    };




    public View getDataRow(int posicao, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);

        View item = inflater.inflate(R.layout.item_comment_detalhes, null);
        Comment comment = activity.getListaCommentarios().get(posicao);

        TextView nomeUser = (TextView) item.findViewById(R.id.name_user_comment);
        TextView commentario = (TextView) item.findViewById(R.id.comment_user);
        Uri uri = Uri.parse("https://farm1.staticflickr.com/2/1418878_1e92283336_m.jpg");
        SimpleDraweeView draweeView = (SimpleDraweeView) item.findViewById(R.id.foto_user_comment);
        draweeView.setImageURI(uri);

        nomeUser.setText(comment.getAuthorname());
        commentario.setText(comment.get_content());
        return item;
    }

    static class ViewHolderLinha{
        SimpleDraweeView draweeView;
        TextView tituloFoto;
        TextView autor;
    }


    public View getFooterView(int position, View convertView,
                              ViewGroup parent) {
        if (position >= serverListSize && serverListSize > 0) {
            // the ListView has reached the last row
            TextView tvLastRow = new TextView(activity);
            tvLastRow.setHint("Reached the last row.");
            tvLastRow.setGravity(Gravity.CENTER);
            return tvLastRow;
        }

        View row = convertView;
        if (row == null) {
            row = activity.getLayoutInflater().inflate(
                    R.layout.progress, parent, false);
        }

        return row;
    }
}

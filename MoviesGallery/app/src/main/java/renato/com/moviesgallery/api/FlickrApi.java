package renato.com.moviesgallery.api;

import java.util.List;
import java.util.Map;

import renato.com.moviesgallery.model.Comment;
import renato.com.moviesgallery.model.Person;
import renato.com.moviesgallery.model.Photo;
import renato.com.moviesgallery.model.Photos;
import renato.com.moviesgallery.model.ResponsePhotos;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.QueryMap;

/**
 * Created by Renato on 26/06/2015.
 */
public interface FlickrApi {
    //exemplo de url
    //https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=f9615aa514c491eeb5d773872650cfc8&tags=movies&format=json&nojsoncallback=1
    //passar no map method,api_key,tags e format
    @GET("/") void buscarImagemTagMovies(@QueryMap Map<String, String> options,Callback<List<Photo>> callback);

    // https://api.flickr.com/services/rest/?method=flickr.photos.comments.getList&api_key=f9615aa514c491eeb5d773872650cfc8&photo_id=18511773973&format=json&nojsoncallback=1
   // @GET("/") void buscarCommentariosImagem(@QueryMap Map<String, String> options, Callback<List<Comment>> callback);

    //https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_[mstzb].jpg
   // @GET("/") void buscarInformacoesUsuario(@QueryMap Map<String, String> options,Callback<Person> callback);

    //usuario
    //https://api.flickr.com/services/rest/?method=flickr.people.getInfo&api_key=39b06b284accf3e6834be415feb395ae&user_id=128323630%40N03&format=json&nojsoncallback=1

    @GET("/")
    ResponsePhotos buscaPhotos(@QueryMap Map<String, String> options);

}

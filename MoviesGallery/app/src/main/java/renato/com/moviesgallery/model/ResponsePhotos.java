package renato.com.moviesgallery.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Renato on 27/06/2015.
 */
public class ResponsePhotos {
    @Expose
    Photos photos;
    @Expose
    private String stat;


    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }
    public Photos getPhotos() {
        return photos;
    }

    public void setPhotos(Photos photo) {
        this.photos = photo;
    }
}

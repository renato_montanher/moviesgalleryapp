package renato.com.moviesgallery.model;

/**
 * Created by Renato on 25/06/2015.
 */
public class Person {
    String id;
    String nsid;
    String ispro;
    int can_buy_pro;
    String iconserver;
    String iconfarm;
    String path_alias;
    int has_stats;
    Username username;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNsid() {
        return nsid;
    }

    public void setNsid(String nsid) {
        this.nsid = nsid;
    }

    public String getIspro() {
        return ispro;
    }

    public void setIspro(String ispro) {
        this.ispro = ispro;
    }

    public int getCan_buy_pro() {
        return can_buy_pro;
    }

    public void setCan_buy_pro(int can_buy_pro) {
        this.can_buy_pro = can_buy_pro;
    }

    public String getIconserver() {
        return iconserver;
    }

    public void setIconserver(String iconserver) {
        this.iconserver = iconserver;
    }

    public String getIconfarm() {
        return iconfarm;
    }

    public void setIconfarm(String iconfarm) {
        this.iconfarm = iconfarm;
    }

    public String getPath_alias() {
        return path_alias;
    }

    public void setPath_alias(String path_alias) {
        this.path_alias = path_alias;
    }

    public int getHas_stats() {
        return has_stats;
    }

    public void setHas_stats(int has_stats) {
        this.has_stats = has_stats;
    }

    public Username getUsername() {
        return username;
    }

    public void setUsername(Username username) {
        this.username = username;
    }
}

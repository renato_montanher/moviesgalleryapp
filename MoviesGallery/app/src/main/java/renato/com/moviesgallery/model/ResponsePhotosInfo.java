package renato.com.moviesgallery.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Renato on 28/06/2015.
 */
public class ResponsePhotosInfo {
    @Expose
    Photo photo;
}

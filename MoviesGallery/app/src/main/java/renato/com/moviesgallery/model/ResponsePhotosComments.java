package renato.com.moviesgallery.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Renato on 28/06/2015.
 */
public class ResponsePhotosComments {
    @Expose
    Comments comments;

    public Comments getComments() {
        return comments;
    }
}

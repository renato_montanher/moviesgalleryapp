package renato.com.moviesgallery;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;

import java.util.ArrayList;
import java.util.List;

import renato.com.moviesgallery.adapter.ListComentariosAdapter;
import renato.com.moviesgallery.adapter.ListImageMovieAdapter;
import renato.com.moviesgallery.controller.CommentsController;
import renato.com.moviesgallery.controller.PhotoController;

import renato.com.moviesgallery.dialog.InfoDialog_;
import renato.com.moviesgallery.listener.EndlessScrollListener;
import renato.com.moviesgallery.model.Comment;
import renato.com.moviesgallery.model.Photo;

/**
 * Created by Renato on 28/06/2015.
 */

@EActivity(R.layout.detalhes)
public class Detalhes extends FragmentActivity{

    @ViewById
    ListView listViewComments;


    @ViewById
    TextView comments_count;
    @ViewById
    TextView titulo;

    @ViewById
    LinearLayout root;
    @ViewById
    SimpleDraweeView photo_movie;

    @Bean
    CommentsController controller;

    @StringRes
    String problemasNoRequest,erroNoServidor;


    @Bean
    ListComentariosAdapter adapter;

    int pagina = 0;
    String idPhoto;

    private List<Comment> listaComentarios = new ArrayList<>();

    @AfterViews
    void afterView() {
        listViewComments.setAdapter(adapter);


        Intent it = getIntent();

        idPhoto = it.getStringExtra("id_photo");
        controller.buscaPhotoComments(idPhoto);
        Uri uri = Uri.parse(it.getStringExtra("url_photo"));
        photo_movie.setImageURI(uri);
        titulo.setText(it.getStringExtra("titulo"));


    }

    @AfterInject
    void afterInject() {

    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
    }

    public void atualizaTelaComResultado(List<Comment> comments) {
        listaComentarios.addAll(comments);
        adapter.notifyDataSetChanged();
        if (listaComentarios.size()>=1) {
            comments_count.setText(listaComentarios.size() > 1 ? listaComentarios.size() + " commentarios." : listaComentarios.size() + " commentario.");
        }else{
            comments_count.setText("sem comentarios");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.inicio, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void trataErrosDeConexao() {

    }

    public void trataErrosDeHTTP(int status) {
        String mensagemId;

        if (status == 400)
            mensagemId = problemasNoRequest;
        else
            mensagemId = erroNoServidor;

        InfoDialog_.builder().infoMessage(mensagemId).build()
                .show(getFragmentManager(), "info_dialog");

    }


    public List<Comment> getListaCommentarios() {
        return listaComentarios;
    }

}
